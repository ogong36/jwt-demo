package com.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;

import com.auth0.jwk.JwkException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) 
        throws NoSuchAlgorithmException, InvalidKeySpecException, JwkException, JSONException, 
            JsonParseException, IOException, IllegalArgumentException, SignatureVerificationException
    {
        System.out.println( "Hello World!" );

        JwtRsa jwt = new JwtRsa();

        /*jwt.testJwtRsa();

        String token = "eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhZGFtIiwiZXhwIjoxNjM3NDAzMDc4LCJpc3MiOiJpbmZvQHdzdHV0b3JpYWwuY29tIiwiZ3JvdXBzIjpbInVzZXIiLCJhZG1pbiJdfQ.Pe9_Dmx4qIveYUJn-_2rpLVRM2QVXsKO376qIVsXLZxaQXSLlBGMmwlcVMcLnBgI0hVxSQ3LzupA82v2k41WjpMnCOzNBq7InHKzOmWrf44wkmZY58BxfoErPh_ROB4hcHsdvVKyH5DPvaAOTCwezzyZ_ESLeovPD-SfAL4ssTE";
        String publicKeyStr = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCzEatIY0JcLmH/arOSZjpyq3Uu3JCnEgC2sixlA3oV1ywW6ACnLCxnCx/atl4Ip0qjKPy2+0D7AQaVzE7oGgn+i+VgZ0U7PquTSPchdGkbBA7ZOa+Twr1av4tIDBpg9TvP0DWJNGQI8OnPqJcnz1cU4xcimj07hurOv58aqkBEPQIDAQAB";
        try {
            jwt.printToken(token, publicKeyStr);
        } catch(JwtException e) {
            System.out.println("Invalid TOKEN------"+e.getMessage());
        }
        catch (Exception e) {
            System.out.println("INVALID TOKEN");
        }//*/

        // Key set from keycloak
        jwt.setKeySetFromFile("keyset.json");

        //Validate 
        String kcToken = loadTextFile("token.json");
        jwt.verifySignature(kcToken);
    }

    static HashMap<String, Object> jsonStringToMap(String jsonStr) throws JsonParseException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(jsonStr, new TypeReference<HashMap<String, Object>>() {});
	}


    static String loadTextFile(String filename) throws IOException {
        String content = "";
        try {
            content = new String(Files.readAllBytes(Paths.get(filename)));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
}
