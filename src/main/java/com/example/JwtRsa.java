package com.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.Iterator;

import com.auth0.jwk.InvalidPublicKeyException;
import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import org.json.JSONArray;
import org.json.JSONObject;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtRsa {
    
    private JSONArray keySet;

    public void testJwtRsa() throws NoSuchAlgorithmException, JwkException {

        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
        keyGenerator.initialize(1024);

        KeyPair kp = keyGenerator.genKeyPair();
        PublicKey publicKey = (PublicKey) kp.getPublic();
        PrivateKey privateKey = (PrivateKey) kp.getPrivate();

        String encodedPublicKey = Base64.getEncoder().encodeToString(publicKey.getEncoded());
        System.out.println("Public Key: ");
        System.out.println(convertToPublicKey(encodedPublicKey));

        String token = generateJwtToken(privateKey);
        System.out.println("TOKEN: ");
        System.out.println(token);
        printStructure(token, publicKey);

        //String kid = "ZqaHE1Pajxs3WxtKvNwjUQXZ0fu3I2BH2BKL_nn6i4I";
        //JwkProvider provider = new UrlJwkProvider("http://localhost:8080/auth/realms/myrealm/protocol/openid-connect/certs");
        //Jwk jwk = provider.get(kid);


    }

    public String generateJwtToken(PrivateKey privateKey) {
		String token = Jwts.builder().setSubject("adam")
				.setExpiration(new Date(System.currentTimeMillis()+3600000))
				.setIssuer("info@wstutorial.com")
				.claim("groups", new String[] { "user", "admin" })
				// RS256 with privateKey
				.signWith(SignatureAlgorithm.RS256, privateKey).compact();
		return token;
	}

    public void printStructure(String token, PublicKey publicKey) throws ExpiredJwtException {
		Jws parseClaimsJws = Jwts.parser().setSigningKey(publicKey)
				.parseClaimsJws(token);

		System.out.println("Header     : " + parseClaimsJws.getHeader());
		System.out.println("Body       : " + parseClaimsJws.getBody());
		System.out.println("Signature  : " + parseClaimsJws.getSignature());
	}

    private String convertToPublicKey(String key){
		StringBuilder result = new StringBuilder();
		result.append("-----BEGIN PUBLIC KEY-----\n");
		result.append(key);
		result.append("\n-----END PUBLIC KEY-----");
		return result.toString();
	}

    public void printToken(String token, String publicKeyStr) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        byte[] publicBytes = Base64.getDecoder().decode(publicKeyStr);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey pubKey = keyFactory.generatePublic(keySpec);

        printStructure(token, pubKey);
    }

    public void publicKeyFromKeySet(String keySetStr) {
        JSONObject keySetObj = new JSONObject(keySetStr).getJSONObject("keys");
        System.out.println(keySetObj.keys().toString());
    }

    String loadTextFile(String filename) throws IOException {
        String content = "";
        try {
            content = new String(Files.readAllBytes(Paths.get(filename)));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    public void setKeySetFromFile(String filename) {
        // Key set from keycloak
        try {
            this.keySet = new JSONObject(loadTextFile(filename)).getJSONArray("keys");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public boolean verifySignature(String token) 
        throws InvalidKeySpecException, InvalidPublicKeyException {

        DecodedJWT jwt = JWT.decode(token);
        if ( !jwt.getAlgorithm().equals("RS256") ) {
            System.out.println("Wrong signature algorithm : " + jwt.getAlgorithm());
            return false;
        }

        String kid = jwt.getKeyId();
        System.out.println("kid="+kid);

        // Key
        Iterator<Object> iter = keySet.iterator();
        JSONObject key = new JSONObject();
        int index = 0;
        while(iter.hasNext()) {
            key = (JSONObject) iter.next();
            //System.out.println(key.get("kid"));
            if(key.get("kid").equals(kid)) {
                break;
            }
            index++;
        }
        
        if(index >= keySet.length()) {
            System.out.println("No key id in the key-set");
            return false;
        }

        //System.out.println("KEY : \n" + key.toString());

        Jwk jwk = Jwk.fromValues(key.toMap()); //(jsonStringToMap(key..toString()));
        //System.out.println("JWK : \n" + jwk.toString());
        //System.out.println("Public Key: \n" + jwk.getPublicKey().toString());

        DecodedJWT kcJwt = JWT.decode(token);
        //System.out.println("Header   :\n"+kcJwt.getAlgorithm());
        System.out.println("Expire at : "+kcJwt.getExpiresAt());
        //System.out.println("Signature:\n"+kcJwt.getSignature());
        Algorithm algorithm = Algorithm.RSA256((RSAPublicKey)jwk.getPublicKey(), null);
        try {
            algorithm.verify(kcJwt);
        }catch(SignatureVerificationException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("Signature verification: OK");
        
        return true;
    }
}
